
Code Structure
==============


GIMS is a web client-server system and so less surprisingly there are two parts of GIMS: The client side is responsible of the user interaction and data visualization and the server part holds the scripts for pre-processing/post-processing information before/after the simulation.

The client side is written in Javascript, while the server side makes use of python. You may ask, whether it would have better to keep the whole application more homogeneous. However, we wanted to have the optimal experience for the end user and making use of already existing libraries in our community, which are mainly written in python. On the one hand using a `javascript-only` framework would have required us to stupidly rewrite big code blocks in javacsript. On the other hand having a minimal frontend solution might have negatively affected the user experience, since communication of big files might significantly reduce the responsiveness of the application. Nowadays, the browser has become a more and more powerful engine by itself: for us a two-component application evolved, naturally.

The following figure may grant you glance on the overall code structure. It also shows the main dependencies of the package on each of the sides.

.. image:: ../images/software_architecture.png

**References to other code projects:**

* Flask: `<https://flask.palletsprojects.com/en/1.1.x/>`_
* ASE: `<https://wiki.fysik.dtu.dk/ase/index.html>`_
* Spglib: `<https://spglib.github.io/spglib/>`_
* threejs: `<https://threejs.org>`_
