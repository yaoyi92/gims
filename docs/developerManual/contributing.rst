How to contribute
=================

General git workflow
--------------------

1. `Open a new issue <https://gitlab.com/gims-developers/gims/-/issues>`_ (either to report a bug or to propose a new feature).
2. Go to the issue and below the description of the issue select *Create merge request* (or alternatively choose just *Create branch*). This will create a new branch and open a merge request. The name of the branch is equal to the name of the issue.
3. Update your local repository by typing in the terminal:

    .. code-block:: bash

      git checkout master
      git pull
      git checkout <name-of-new-branch>

4. Now, make your changes. Please commit often and early. Do not commit a huge amount of work at once. Please add a descriptive commit message, so everyone gets an idea about your changes/new implementations. This keeps the process transparent for all contributors. To commit you can, e.g., use:

    .. code-block:: bash

      git add <file-name-1> <file-name-2>
      git commit

5. Push your changes into the remote branch:

    .. code-block:: bash

      git push -u origin <name-of-new-branch>

6. Once you have finished your changes, go back to the browser to the gims GitLab. You will need an approval of your merge request by an eligible user. If your merge request is approved, go to your merge request and select *Merge*.
7. If you have implemented a new or changed an existing feature, please document your changes in the :ref:`changelog <changelog>`. Further, please also document how to use your new feature in the docs. Please provide information for the user manual.
8. Finally, add tests for the new functionalities for both the client and server side!

Adding support for a new Code
-----------------------------

If you plan to add support for a new electronic structure code, there are only a countable number of steps to do. In what follows gives a rough overview.

Client Side
+++++++++++

1. Add the code icon to ``client/img`` (you'll find examples already there) and add it in the ``index.html`` to the div ``code-selection-box``. For the code name use the format ``<codeName-logo.png>``. Don't use special characters for the code name.
2. Add the definition of the fields needed for the Control Generator. Please use the following name for it: ``Fields_<codeName>.js``. Examples for this file can be found in the folder ``client/src/control-generator-mod``. Import the ``Fields_<codeName>.js`` in the ``AllFields.js`` file.
3. Please add a parser for the output files of your code to the ``output-analyzer-mod``. The prototype is defined as ``Output.js`` in ``client/src/output-analyzer-mod``. Examples for existing supported code parser are in this folder, too. Please add your new parser object to the function ``getOutputInstance`` in ``utils.js`` in the ``output-analyzer-mod`` folder.

Server Side
+++++++++++

1. Please make sure if ASE supports your code. GIMS relies on the auto-detection of the structure-data files. If this is missing, we recommend to add the support for your code first there.
2. Please provide a code class for the new code in the ``app/gims/codes`` directory. A prototype code class and examples for other codes can be found there, too.

Contact
-------

If you experience any problems during the contribution process or if you have any suggestions, feel free to contact me directly via: kokott@fhi-berlin.mpg.de
