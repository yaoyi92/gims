

Installation
============

If you ended up here to install a local version of GIMS (e.g., to use it offline), make sure that the desktop version provided for download on the GIMS main page is not enough for you. The steps described here require some setup patience. However, you are free to follow them. At the end, you will be able to run the most recent developer version from any branch in the gitlab. So if you can't wait for the next release ... please go ahead!

The installation involves the following three steps:
    1. Download the Code
    2. Build the client in the ``client`` folder (frontend code written in javascript). The result will be copied to ``app/gims/static``.
    3. Install the GIMS python application (backend code written in python) in the folder ``app``.

Obtaining the Code
------------------

To obtain the source code, please clone the repository (make sure you have `deployed you ssh keys on gitlab <https://docs.gitlab.com/ee/ssh/>`_):

.. code-block:: bash

    git clone git@gitlab.com:gims-developers/gims.git

Prerequisites
-------------

We make use of several packages to bundle/build the app and (python) libraries for actually running the app. The most convienient way to quickly install all needed packages is to use the popular package managers ``npm`` and ``pip`` for ``javascript`` and ``python`` packages, respectively. There is a lot of material out there in the so called internet about installing these packages on the OS of your choice. In what follows we assume that they are installed on your system.

A quick overview what is needed to follow the installation instructions:

    * ``node`` (node.js includes the package manager ``npm``)
    * ``python3.6`` (this will include ``pip``)

If you hav install the above, then, the following package managers should be already in place:

    * npm
    * pip

.. _build-app:

Building the Client
-------------------

To bundle and finally build the client side of the app we use `parcel <https://parceljs.org>`_. Please enter the following in your terminal:
::

    npm install -g parcel-bundler

After that we are ready to install the project. Go to the folder ``gims/client/`` and type:
::

    npm install

``npm`` will automtically install the needed modules. The output of the build process is saved to the ``gims/app/static`` folder.

There are two modes availabe:

    1. development mode: This mode uses the ``parcel watch`` option. Parcel rebuilds the app automatically, whenever a change to the javascript files has been made. There is no need to manually recompile the app over and over again while developing. Please type for that:
    ::

        npm run dev

    2. production mode: This mode uses the ``parcel build`` option. It builds the app once and that's it. To use this mode, type:
    ::

        npm run build

To check whether the build process was successfull, please check if the folder ``gims/app/static`` has any content. If it has we are finished with building of the client side of the application.


Install the GIMS python application
-----------------------------------

We now turn to the server part of your local installation. Please change to the folder ``gims/app``. You should see now some python files. To isolate your application from dependencies from the rest of your machine we recommend to use python virtual environment. We will briefly discuss the setup of such an environment, first.

Setup a virtual environment
"""""""""""""""""""""""""""

Please install ``virtualenv`` with:
::

    pip install virtualenv

Now, setup a new virtual environment:
::

    python3 -m venv env

The settings of this virtual environment are saved in the folder `gims/app/env`.

Activate the virtual environment with:
::

    source env/bin/activate

Install GIMS app
""""""""""""""""

GIMS can be installed like any other python package. Just got to ``app`` folder and type :
::

    pip install -e .[dev]

All dependencies should be installed automatically. A list for all GIMS dependencies can be found in the ``setup.py`` file in the ``app`` folder.

Start the flask server
""""""""""""""""""""""

For the use of GIMS on your local machine we will use the development server from Flask. Please go to the folder ``app`` in the `gims` folder and type the following in the terminal:
::

    export FLASK_APP=gims
    flask run

To check if everything works as expected go to your browser and visit (enter in the URL bar):
::

    localhost:5000

That's it! It was a long road to go, though.
