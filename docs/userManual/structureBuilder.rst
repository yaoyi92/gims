
Structure Builder
=================

The *Structure Builder* is an app for viewing and manipulating atomic structures, such as molecules and solids.

Import structure files
----------------------

In the right upper corner you find the *Import* button. The *Structure Builder* first tries to detect the file format, automatically. If it fails, you can choose among the following formats:

+------------------+------------------------------------------+
| Code             | File Detection                           |
+==================+==========================================+
| FHI-aims         | File ends with ``*.in``                  |
+------------------+------------------------------------------+
| Exciting         | File name ``input.xml``                  |
+------------------+------------------------------------------+
| CIF-file         | File ends with ``*.cif``                 |
+------------------+------------------------------------------+
| XYZ-file         | File ends with ``*.xyz``                 |
+------------------+------------------------------------------+
| VASP             | File name ``POSCAR``                     |
+------------------+------------------------------------------+
| quantum espresso | File contains ``&system`` or ``&SYSTEM`` |
+------------------+------------------------------------------+

3D Structure Viewer
-------------------

If the file has been imported successfully, the structure of atoms should be displayed. You can inspect the structure in the following ways:

* Rotate the structure: Hold left mouse and move
* Move the structure: Hold right mouse and move
* Get atom information: Hover over atom

Structure Analysis
""""""""""""""""""

Further, you can easily do some structure analysis:

* Measure atom distance: Hold shift and select two atoms
* Measure angle of three atoms: Hold shift and select three atoms
* Measure torsion angle of four atoms: Hold shift and select four atoms
* De-select atoms: Click right

In the left upper corner the button **View options** allows to further alter the displayed content, e.g. you can switch on and off the display of the unit cell, bonds, lattice vectors and so on.

Changing Species Colors
"""""""""""""""""""""""

The colors of the species and their respective names are shown on in the middle on the left side of the structure viewer. Clicking on the color symbol allows to select a new color for the species. To reset the colors of all elements click on the *circle arrow*.

Structure Manipulation
""""""""""""""""""""""

There are two ways to manipulate your uploaded structure: 1) 3D Structure Viewer 2) Side Panel.

    1. Structure Viewer: To edit the structure click on the button **Edit structure**. You can now perform the following actions:

        * Move atoms: Click on an atom and hold and move atom (drag and drop)
        * Delete atom: Press D and click on atom

    2. Side Panel: To the right  of the 3D structure viewer you find editable fields. You can:

        * Get structure information about your system, such as space group, lattice parameters, occupied Wyckoff positions, etc.
        * Change or delete lattice vectors. When changing lattice vectors you can choose either to scale all atom position with the lattice vectors. To activate this feature click on the checkbox below the lattice vector fields.
        * Create supercell. By filling the fields with integer number in the **Create Supercell** section you can create a supercell with lattice vectors multiples of the initial length.
        * Create the primitive cell, that is, find the smallest possible unit cell (implemented via spglib).
        * Create a slab from a crystalline structure, by entering the Miller indices of the slab surface, the number of atomic layers and the thickness of vacuum needed. After creation, there is also a possibility to terminate the slab with the layers of desired chemical composition. **NOTE:** as of now, the layers are distinguished by the chemical composition, and not by the symmetry of the structure. Also, the termination is achieved by removing the layers from the existing slab until the required layer is found, so the terminated slab will have `less` number of layers than the original.
        * Change basis atoms: All properties of the basis atoms can be edited: atom position atom species. Moreover, by **clicking on the species color**, you can **constrain the atom positions** (e.g. for relaxation calculations) and you can set an **initial spin moment** on that atom (if supported by the corresponding code). Clicking on the trash bin deletes the atom from the structure. To add a new atom click on the **New atom** button.

        .. figure:: ../images/basisAtoms.png
           :align: center
           :scale: 60 %

           Click on the species color behind the element symbol field to constrain the element for a relaxation and/or to set the initial moment of the corresponding atom.

You can follow the **number of changes** of your structure in the top center of the 3D structure viewer (below the button **Edit structure**). Click on the arrows to the left and right to undo and redo your last actions, respectively.

3D Brillouin Zone Viewer
------------------------

In case of a periodic structure (structure has three lattice vectors) the corresponding Brillouin zone (BZ) and the band path according to Setjawan/Curtarolo is shown.


Export Structure Files
----------------------

To export your structure click on the **Export** button. The export format will be according your selected code, i.e. if you have selected FHI-aims, a geometry.in file will be downloaded to your download folder.

Please note: Due to browser security restrictions, we can only save files to the download folder. To change the download folder please change it in the settings of your browser.

Reset Structure
---------------

To erase all structural data click on the button "Reset" in the top right corner. Be careful: It will reset all changes and steps before. You will have to confirm this step.
