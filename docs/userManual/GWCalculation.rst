GW Calculation
==============

The `GW approximation <https://www.frontiersin.org/articles/10.3389/fchem.2019.00377/full>`_ is now a widely used method
in computational physics and chemistry for calculating electronic properties of materials, such as the electronic band
structure and optical excitation spectra. In this method, the self-energy of the system is approximated by the product
of the Green's function (*G*) and the screened Coulomb interaction (*W*). The Green's function describes the probability
of finding an electron in a particular state, while the screened Coulomb interaction describes the interactions between
electrons mediated by the surrounding atoms.

The method is readily available in FHI-aims and is exposed in GIMS through the
`GW calculation app <https://gims-dev.ms1p.org/static/index.html#GWCalculation-workflow#StructureBuilder>`_.
Similar to *Band Structure* calculation app, this app connects these elemental steps:

1. :doc:`structureBuilder`;

2. :doc:`controlGenerator`;

3. Input files downloader;

4. :doc:`outputAnalyzer`.

The main difference with the :doc:`bandStructure` calculation is the Control Generator step, where the most important GW keywords
are exposed as inputs. Let's go through them below.

.. image:: ../images/GW_CG.png
   :alt: GW calculation Control Generator screenshot
   :align: center

The first input, *Perturbative quasiparticle correction*, corresponds to the keyword ``qpe_calc`` which turns the GW
calculation on. Its value can be chosen from a handful of options, which are documented both in the FHI-aims manual and within
GIMS (hover the mouse over the label to see the tip). As the allowed values for ``qpe_calc`` differ depending on the
periodicity of the structure, and in compliance with *explicit is better than implicit* principle, a sanity check
of the validity of the chosen value is implemented.

There are two more required keywords at this step. First, the way of *Analytic continuation of self-energy*
to the real frequency axis has to be chosen from two variants: ``two-pole`` fitting and ``Pade`` approximation (see the
FHI-aims manual for the nice theoretical explanation). Also, if the system is periodic, the k-point density for the
``GW band structure`` calculation must be explicitly set.

The optional parameters that can be set include the *number of frequency points* on the imaginary axis to be used for
analytical continuation. Other parameters can be set in *Extra keywords* section which is the last in the
Control Generator. However, no sanity checks are run on this input, and the user themselves is responsible to creating
the meaningful and non-contradictory *control.in* file while using *Extra keywords* section.

The last input in the GW section can be used to prepare canonical DFT inputs in order to compare DFT and GW results,
thus making it a kind of a workflow. When *Also prepare DFT inputs* is checked, the resultant archive contains two sets
of input files: one for DFT calculation without GW approximation, and the other for the GW calculation, as well as the
README file explaining next step. Upon finishing the calculations, the outputs can by analyzed simultaneously by
archiving them and providing the archive to the Output Analyzer. There, band structures as well as densities of
electronic states can be visualized and compared.