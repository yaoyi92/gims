MD Calculation
==============

_Currently only working for FHI-aims_ 

A Molecular Dynamics (MD) simulation is used to study the dynamic behavior of particles by integrating equations of motion. The internal module of FHI-aims is used; two thermostats can be chosen for the input: NVE (microcanonical ensemble) and NVT by Bussi, Donadio and Parrinello (canonical ensemble). You have to specify: 

 * the thermostat and additional ensemble specific parameters;
  
 * the run time in ps;
 
 * the time step in ps;
 
 * the velocity initialization temperature in K.
