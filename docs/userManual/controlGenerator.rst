
Control Generator
=================

The *Control Generator* allows to define all necessary numerical settings to run a calculation.

Note: This part is highly Code dependent. Please choose your preferred code in advance in the top right corner (**Choose you code** section). Changing the code after completing the form will delete all progress.

By hovering over the settings text you will get additional information to the right side of the corresponding item.

Downloading control files
-------------------------

If all mandatory fields are filled, you can click on **Generate and download input file(s)**. Then, the control generator creates the input file(s) (input files that contain numerical settings). This also includes corresponding basis sets or basis setting, even if not explicitly requested. (The control generator makes use of code internal defaults as much as possible.)

**FHI-aims:** the ``control.in`` file is generated and the chosen species defaults (light, tight etc.) are pasted into the file as well.

**Exciting:** the ``input.xml`` is generated and all needed species files (``<species>.xml``) are collected, too.

All input files are packed in to a single ``input_files.tar`` file, which is finally saved to your download directory.
