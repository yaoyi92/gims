Main Page
=========

The figure below shows the main page of the GIMS application. An example can be found `here
<https://gims.ms1p.org>`_.

.. image:: ../images/MainPage.png
  :width: 98%

1. **GIMS Button:** The GIMS button is the *home button*, that is, clicking on this icon always returns to the apps
   main page.

2. **Desktop application:** Click on this icon to get a description how to download and install the GIMS offline
   executable.

3. **Code Selection:** By clicking on the corresponding code icon the underlying behavior of the apps is adapted to the
   selected code.

4. **GIMS version selector:** by clicking this combobox one of the GIMS deployed versions can be accessed:

    - `development version <https://gims-dev.ms1p.org>`_, deployed automatically from the HEAD of the ``master``
      branch of the git repository;

    - `stable version <https://gims.ms1p.org>`_, deployed from the latest release;

    - and `fallback version <https://gims.ms1p.org/fallback/>`_, namely version 1.0.9.

5. **Settings:** The settings menu enables to change display properties, such as the number of shown floating point digits.

6. **Feedback and User Manual:** This *Feedback* icon redirects to the issue tracker of the GIMS repository. You can
   leave suggestions, improvements, and bug reports, here. The *User Manual* icon redirects you to the current web site.

7. **App Selector:** Here, you can select the individual apps. They are described in detail in the subsequent chapters.
   Hovering over the question mark displays a short description below the app icon. It is further subdivided into two
   parts: a) workflow apps (*Simple Calculation* and *Band Structure*), and b) elemental apps (*Structure Builder*,
   *Control Generator*, and *Output Analyzer*).




