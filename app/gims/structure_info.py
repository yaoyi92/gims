from ase.atoms import Atoms
import spglib as spg
import numpy as np


class StructureInfo:
    """Wrapper to get supportive information about the structure"""
    def __init__(self, system, sym_thresh):
        """Initializes the class from an ASE atoms object.

        Parameters:

        system: ASE atoms object

        sym_thresh: float
            Symmetry threshold for determining Bravais lattice (ASE feature) and
            Space group information (spglib feature).
        """
        self.info = {}
        self.atoms = system
        self.add_info("n_atoms", len(system), "Number of atoms")
        self.add_info("formula", system.get_chemical_formula(), "Chemical formula")

        if all(system.pbc):
            lattice = system.get_cell()[:]
            positions = system.get_scaled_positions()
            numbers = system.get_atomic_numbers()
            magmoms = system.get_initial_magnetic_moments()
            spg_cell = (lattice, positions, numbers, magmoms)
            self.add_info(
                "unit_cell_parameters",
                np.around(system.cell.cellpar(), 12),
                "Lattice parameters <br> (a, b, c, \u03B1, \u03B2, \u03B3)",
            )
            bravais = system.cell.get_bravais_lattice(eps=sym_thresh)
            self.add_info(
                "bravais", "{} {}".format(bravais.longname, bravais), "Bravais Lattice"
            )
            dataset = spg.get_symmetry_dataset(spg_cell, symprec=sym_thresh)
            if dataset:
                prim_lattice, prim_scaled_positions, prim_numbers = spg.find_primitive(
                    spg_cell, symprec=sym_thresh
                )
                self.primitive = Atoms(
                    cell=prim_lattice,
                    scaled_positions=prim_scaled_positions,
                    numbers=prim_numbers,
                    pbc=True,
                )
                conv_lattice, conv_scaled_positions, conv_numbers = spg.standardize_cell(spg_cell, symprec=sym_thresh)
                self.conventional = Atoms(
                    cell=conv_lattice,
                    scaled_positions=conv_scaled_positions,
                    numbers=conv_numbers,
                    pbc=True,
                )
                self.add_info("sym_thresh", sym_thresh, "Symmetry Threshold")
                self.add_info("spacegroup", dataset["number"], "Spacegroup number")
                self.add_info("hall_symbol", dataset["hall"], "Hall symbol")
                self.add_info(
                    "occupied_wyckoffs",
                    np.unique(dataset["wyckoffs"]),
                    "Occupied Wyckoff positions",
                )
                self.equivalent_atoms = dataset["equivalent_atoms"]
                self.unique_equivalent_atoms = np.unique(dataset["equivalent_atoms"]) + 1
                self.add_info(
                    "equivalent_atoms",
                    np.unique(dataset["equivalent_atoms"]) + 1,
                    "Unique equivalent atoms",
                )
                self.add_info(
                    "is_primitive", len(system) == len(prim_numbers), "Is primitive cell?"
                )

    def add_info(self, key, value, info_str=""):
        """Add new key to info dict"""

        v = value
        if isinstance(value, np.ndarray):
            v = value.tolist()
        self.info[key] = {"value": v, "info_str": info_str}

    def get_info(self):
        """Return the current info dictionary"""
        return self.info

    def __str__(self):
        str_str = "System Info\n" + "-" * 14 + "\n"
        for key, idict in self.info.items():
            # print(key,idict)
            if isinstance(idict["value"], (list, np.ndarray)):
                fmt_str = "{:30}: " + "{} " * len(idict["value"]) + "\n"
                str_str += fmt_str.format(idict["info_str"], *idict["value"])
            else:
                fmt_str = "{:30}: {}\n"
                str_str += fmt_str.format(idict["info_str"], idict["value"])
        return str_str
