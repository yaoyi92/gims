import os
from pathlib import Path

import numpy as np

from ase import __version__ as ase_version
from ase.calculators.aims import Aims
from ase.calculators.calculator import kpts2mp
from ase.dft.kpoints import resolve_kpt_path_string, kpoint_convert
from gims.inputs.codeinputs import CodeInputs
from gims.structure import Structure


class FHIaims(CodeInputs):

    gw_keys = ['qpe_calc', 'anacon_type', 'frequency_points']

    def __init__(self, data, species_dir, session=None):
        """FHI-aims Code class

        Provides the specific methods to prepare the corresponding input files.

        Parameters:

        data (dict):
            Dictionary generated from form sent by client.
        species_dir (str|Path):
            Path to the FHI-aims species defaults directory.
        """
        super(FHIaims, self).__init__(session)
        self.info = {"references": [], "DownloadInputFilesPage": {}}
        self._has_structure = "structure" in data
        self.species_dir = species_dir
        self.calc = None
        self.error = None

        if self._has_structure:
            self.structure = Structure.from_dict(data["structure"])
            if np.any(self.structure.get_initial_magnetic_moments()):
                data["form"]["spin"] = "collinear"
        else:
            # make dummy structure object
            is_periodic = any([x in data["form"] for x in ("k_grid", "k_grid_density")])
            self.structure = Structure.from_form(data["form"], is_periodic)

        # Band structure calculations with HSE06+SOC should have exx_band_structure_version set; #89
        if ('hse06' in data['form']['xc']
                and 'include_spin_orbit' in data['form']
                and 'bandStructure' in data['form']):
            data['form']['exx_band_structure_version'] = '1'

        if "relativistic" not in data["form"]:
            data["form"]["relativistic"] = "atomic_zora scalar"

        self.is_gw_workflow = 'needDFTInputs' in data['form']
        if self.is_gw_workflow:
            data['form'].pop('needDFTInputs')

        # MD inputs
        self.is_md = 'MD_run' in data['form']
        if self.is_md:
            time = data['form'].pop('MD_run_time')
            ensemble = data['form']['MD_run']
            params = data['form'].pop('MD_run_params', [])
            data['form']['MD_run'] = [time, ensemble] + params

        self.control = data['form']

    def set_structure(self, structure):
        """ Changes the structure for the inputs without rebuilding all the inputs
        Args:
            structure (Structure): a structure to set
        """
        self.structure = structure
        self.control["species"] = list(set(self.structure.get_chemical_symbols()))
        if np.any(self.structure.get_initial_magnetic_moments()):
            self.control["spin"] = "collinear"

    def set_control(self, key, value):
        """ Sets control generator key to a predefined value
        Args:
            key (string): an FHI-aims key
            value: a predefined value
        """
        self.control[key] = value

    def write_inputs(self, input_dir):
        """ Write input files to a given directory
        Args:
            input_dir (pathlib.Path): input directory
        """
        # get the function object
        calc_keys = self.calc_from_form(self.structure, self.control, self.species_dir.as_posix())
        if self.is_gw_workflow:
            self._write_inputs(input_dir / 'GW', calc_keys)
            calc_keys_dft = {k: v for (k, v) in calc_keys.items() if k not in self.gw_keys}
            self._write_inputs(input_dir / 'DFT', calc_keys_dft)
            with open(input_dir / "README.txt", "w") as f:
                s = """\
    After all calculations are finished, you can archive everything in the directory with the following `tar` command:
        $ tar cvzf workflow.tar.gz */
    and provide the resultant gzip archive to the Output Analyzer"""
                f.write(s)
        else:
            self._write_inputs(input_dir, calc_keys)

    def _write_inputs(self, input_dir, calc_keys):
        if ase_version > "3.22.1":
            self._write_inputs_dev(input_dir, calc_keys)
        else:
            self._write_inputs_stable(input_dir, calc_keys)

    def _write_inputs_dev(self, input_dir, calc_keys):
        """ A subroutine used with dev version of ASE (as dev and 3.22 are backwards incompatible)
        Args:
            input_dir (pathlib.Path): input directory
            calc_keys (dict): a dict of cal control keys
        """
        # setup Aims calculation
        self.calc = Aims()
        self.calc.directory = Path(input_dir)
        try:
            self.calc.template.write_input(self.calc.directory, self.structure, calc_keys, [])
            (self.calc.directory / "parameters.ase").unlink(missing_ok=True)
            if not self._has_structure:
                # delete dummy object
                (self.calc.directory / "geometry.in").unlink(missing_ok=True)
        except (FileNotFoundError, RuntimeError):
            self.error = "FileNotFoundError"

    def _write_inputs_stable(self, input_dir, calc_keys):
        """ A subroutine used with current version of ASE (3.22)
        Args:
            input_dir (pathlib.Path): input directory
            calc_keys (dict): a dict of cal control keys
        """
        # setup Aims calculation
        if 'k_grid_density' in calc_keys:
            # old ASE version, just calculate k_grid
            k_grid_density = calc_keys.pop('k_grid_density')
            calc_keys['k_grid'] = tuple(kpts2mp(self.structure, k_grid_density))

        self.calc = Aims(**calc_keys)
        self.structure.set_calculator(self.calc)
        self.calc.directory = Path(input_dir)
        self.error = None
        print(calc_keys)
        try:
            self.calc.write_input(self.structure)
            os.remove(os.path.join(self.calc.directory, "parameters.ase"))
            if not self._has_structure:
                os.remove(os.path.join(self.calc.directory, "geometry.in"))
        except FileNotFoundError:
            self.error = "FileNotFoundError"

    def calc_from_form(self, struct, form_dict, species_dir):
        """Generates the ASE calculator from the control-generator form.

        Parameters:

        struct: ASE atoms object

        formDict: JSON object
            Dictionary of the control generator form.

        species_dir: String (Path)
            Path to the Species root dir.
        """
        calc_keys = {}
        for key in form_dict.keys():
            if key == "basisSettings":
                calc_keys["species_dir"] = os.path.join(species_dir, form_dict[key])
            elif key in ("bandStructure", "GWBandStructure"):
                # print(key, formDict[key])
                calc_keys["output"] = self.prepare_band_input(struct.cell, density=int(form_dict[key]))
            elif key == "species":
                continue
            else:
                # Only use this if 'key' is already proper FHI-aims keyword
                calc_keys[key] = form_dict[key]
        return calc_keys

    def prepare_band_input(self, cell, density=20):
        """
        Prepares the band information needed for the FHI-aims control.in file.

        Parameters:

        cell: ase.cell.Cell
            ASE cell object
        density: int
            Number of kpoints per Angstrom. Default: 20
        """
        self.get_band_path_info(cell)
        bp = cell.bandpath()
        # print(cell.get_bravais_lattice())
        r_kpts = resolve_kpt_path_string(bp.path, bp.special_points)

        lines_and_labels = []
        for labels, coords in zip(*r_kpts):
            dists = coords[1:] - coords[:-1]
            lengths = [np.linalg.norm(d) for d in kpoint_convert(cell, skpts_kc=dists)]
            points = np.int_(np.round(np.asarray(lengths) * density))
            # I store it here for now. Might be needed to get global info.
            lines_and_labels.append(
                [points, labels[:-1], labels[1:], coords[:-1], coords[1:]]
            )

        bands = []
        for segment in lines_and_labels:
            for points, lstart, lend, start, end in zip(*segment):
                bands.append(
                    "band {:9.5f}{:9.5f}{:9.5f} {:9.5f}{:9.5f}{:9.5f} {:4} {:3}{:3}".format(
                        *start, *end, max(points, 2), lstart, lend
                    )
                )

        return bands
