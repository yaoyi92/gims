import sys

from gims.inputs.FHIaims import FHIaims
from gims.inputs.Exciting import Exciting
from gims.inputs.codeinputs import CodeInputs


def inputs_cls(step_def):
    """ Returns a CodeInputs-derived class based on the code entry in step definition

    Args:
        step_def (str|dict): a ControlGenerator step definition
    """
    code_name = None
    if isinstance(step_def, str):
        code_name = step_def
    elif isinstance(step_def, dict):
        if 'control' in step_def:
            code_name = step_def['control']['code']
        elif 'code' in step_def:
            code_name = step_def['code']

    if code_name is None:
        raise KeyError('Could not find the code name in the workflow step definition')

    try:
        inputs_cls_obj = getattr(sys.modules[__name__], code_name)
    except AttributeError as ex:
        raise Exception(f'Did not find code named {code_name}') from ex

    return inputs_cls_obj
