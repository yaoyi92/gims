from gims import constants


class CodeInputs:
    """Base class for any supported code by GIMS.

    This base class provides the mandatory methods needed within GIMS.
    """
    def __init__(self, session=None):
        self.info = None
        self.session = session if session is not None else {}

    def get_band_path_info(self, cell):
        """Return HTML string of band path information"""

        sym_thresh = self.session.get("symThresh", constants.sym_thresh)
        header = "<h3>Band path information</h3>"
        longname = f"Bravais Lattice: {cell.get_bravais_lattice(eps=sym_thresh).longname} "
        description = cell.get_bravais_lattice(eps=sym_thresh).description()
        band_info = header + longname + description + "\n\n"
        self.info["DownloadInputFilesPage"]["bandInfo"] = band_info

    def calc_from_form(self, struct, form_dict, species_dir):
        """Should contain the logic how to prepare the input files based on the control
        generator form and structure data in json format.

        Parameters:

        struct: ASE atoms object

        formDict: JSON object
            Dictionary of the control generator form.

        species_dir: String (Path)
            Path to the Species root dir.
        """
        raise NotImplementedError("Please implement this method for your code")

    def prepare_band_input(self, cell, density=20):
        """Should contain the logic to request the calculation of the band structure
        along a certain path.

        Parameters:

        cell: object
            ASE cell object
        density: int
            Number of kpoints per Angstrom. Default: 20
        """
        raise NotImplementedError("Please implement this method for your code")
