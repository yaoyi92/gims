import os
import shutil

import numpy as np
from ase.dft.kpoints import resolve_kpt_path_string, kpoint_convert

from gims.inputs.codeinputs import CodeInputs
from gims.structure import Structure


class Exciting(CodeInputs):
    def __init__(self, data, input_dir, species_dir, session=None):
        """Exciting Code class

        Provides the specific methods to prepare the corresponding input files.

        Parameters:

        data: directory
            Dictionary generated from form sent by client.
        input_dir: string
            Path to the directory, where input can be written to.
        species_dir: string
            Path the exciting species.
        """

        super(Exciting, self).__init__(session)
        self.info = {"references": [], "DownloadInputFilesPage": {}}

        if "structure" in data:
            structure = Structure.from_dict(data["structure"])
        else:
            is_periodic = "nkgrid" in data["form"]
            structure = Structure.from_form(data["form"], is_periodic)

        self.calc_from_form(structure, data["form"], species_dir)
        os.makedirs(os.path.dirname(input_dir))
        structure.calc.dir = input_dir
        structure.calc.write(structure)

        if not "structure" in data:
            self.insert_comment(
                "This is just a dummy structure! Please modify it!",
                "structure",
                os.path.join(input_dir, "input.xml"),
            )

        species = self.get_species(structure)
        for s in species:
            fname = s + ".xml"
            shutil.copyfile(
                os.path.join(species_dir, fname),
                os.path.join(structure.calc.dir, fname),
            )

        self.error = None

    @staticmethod
    def get_species(structure):
        """Get unique list of species.

        Parameters:

        structure: ASE atoms object
        """
        species = []
        for s in structure.get_chemical_symbols():
            if s not in species:
                species.append(s)

        return species

    @staticmethod
    def insert_comment(comment, element, filename):
        """Insert a comment in the xml tree."""
        import xml.etree.ElementTree as ET

        doc = ET.parse(filename)
        crystal = doc.getroot().find(element)
        crystal.insert(0, ET.Comment(comment))

        doc.write(filename)
        # print(os.listdir(os.path.dirname(filename)))

    def calc_from_form(self, struct, form_dict, species_dir):
        """Generates the ASE calculator from the control-generator form.
        
        Parameters:

        struct: ASE atoms object

        formDict: JSON object
            Dictionary of the control generator form.

        species_dir: String (Path)
            Path to the Species root dir.
        """
        from ase.calculators.exciting import Exciting

        groundstate_keys = [
            "xctype",
            "rgkmax",
            "ngridk",
            "tforce",
            "epsengy",
            "epschg",
            "epsforcescf",
            "epspot",
        ]

        groundstate = {}

        paramdict = {"title": {"text()": "{}".format(struct.get_chemical_formula())}}
        for key in form_dict.keys():
            if key == "ngridk":
                ks = [str(x) for x in form_dict[key]]
                groundstate[key] = " ".join(ks)
            elif key in groundstate_keys:
                groundstate[key] = str(form_dict[key])
            elif key == "species":
                continue
            elif key == "relax":
                paramdict["relax"] = {}
            elif key == "dos":
                paramdict["dos"] = {
                    "winddos": f"{float(form_dict[key][0])} {float(form_dict[key][1])}",
                    "nwdos": f"{form_dict[key][2]}",
                    "lmirep": "true",
                }
            elif key == "bandStructure":
                continue
            else:
                print("key not found", key)

        paramdict["groundstate"] = groundstate
        calc = Exciting(speciespath=".", paramdict=paramdict)

        if "bandStructure" in form_dict:
            self.prepare_band_input(struct.cell, calc, density=int(form_dict[key]))

        struct.set_calculator(calc)

    def prepare_band_input(self, cell, calc, density=20):
        """
        Prepares the band information needed for the exciting input.xml file.

        Parameters:

        cell: object
            ASE cell object
        calc: object
            ASE calculator
        density: int
            Number of kpoints per Angstrom. Default: 20
        """

        self.get_band_path_info(cell)
        bp = cell.bandpath()
        r_kpts = resolve_kpt_path_string(bp.path, bp.special_points)
        length = 0
        point = []
        for labels, coords in zip(*r_kpts):
            dists = coords[1:] - coords[:-1]
            lengths = [np.linalg.norm(d) for d in kpoint_convert(cell, skpts_kc=dists)]
            length += sum(lengths)

            for label, coord in zip(labels, coords):
                point.append({"coord": "{} {} {}".format(*coord), "label": label})

        n_points = np.int_(np.round(length * density))
        bandstructure = {"plot1d": {"path": {"steps": str(n_points), "point": point}}}

        calc.paramdict["properties"] = {"bandstructure": bandstructure}
