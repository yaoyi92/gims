""" A class for designed workflow creation
"""

import sys
import shutil
from pathlib import Path
from tempfile import TemporaryDirectory
from warnings import warn

from gims import SPECIES_DIR
from gims.structure import Structure
from gims.inputs import inputs_cls


def workflow_step(step_def, parent):
    """ A workflow step factory function

    Args:
        step_def (dict): step definition from designed workflow
        parent (Workflow): a parent workflow

    Returns:
        WorkflowStep child class instance
    """
    step_type = step_def.pop('type')
    try:
        wf_step_cls = getattr(sys.modules[__name__], step_type)
    except AttributeError:
        warn(f"{step_type} class not found in workflow.py. Using generic WorkflowStep instead")
        wf_step_cls = WorkflowStep
    return wf_step_cls(step_def, parent)


class WorkflowStep:
    def __init__(self, step_def, parent):
        self.workflow = parent
        self.step_id = step_def['id']
        self.properties = step_def['properties']
        self.idx = None

    def run(self):
        """Runs a workflow step. Needs to be implemented in the children"""
        raise NotImplementedError


class StructureBuilder(WorkflowStep):
    def run(self):
        """Runs a Workflow step"""
        struct_json = self.properties.get('structure', None)
        if struct_json is None:
            raise ValueError("No structure present in StructureBuilder step")
        self.workflow.properties['structure'] = Structure.from_dict(struct_json)


class CalculationStep(WorkflowStep):
    def run(self):
        control_json = self.properties.get('control', None)
        code_name = control_json.get('code', None)
        species_dir = Path(__file__).parent / SPECIES_DIR / code_name

        if control_json is None:
            raise ValueError("No control generator form is present in Calculation step")
        self.workflow.properties['control'] = inputs_cls(control_json)(control_json, species_dir)


class DFTCalculation(CalculationStep):
    pass


class GWCalculation(CalculationStep):
    pass


class Convergence(WorkflowStep):
    def run(self):
        if 'convergence' not in self.workflow.properties:
            self.workflow.properties['convergence'] = {}
        # preprocess convergence parameters
        if 'k_grid' in self.properties['convergence']:
            self.properties['convergence']['k_grid'] = [[int(x) for x in s.split('x')]
                                                        for s in self.properties['convergence']['k_grid']]
        self.workflow.properties['convergence'].update(self.properties['convergence'])


class Workflow:
    """Designed workflow"""
    def __init__(self, definition):
        self.properties = definition['properties']
        self.sequence = []

        for i, step_def in enumerate(definition['sequence']):
            self.sequence.append(workflow_step(step_def, self))
            self.sequence[-1].idx = i

    def run(self, directory):
        """Runs a workflow. The result is a set of prepared input files
        Args:
            directory (str) - a directory to write the input files to
        """
        for step in self.sequence:
            step.run()
        # now we should have everything in place, let's build inputs
        code_inputs = self.properties['control']
        code_inputs.set_structure(self.properties['structure'])
        errors = []
        temp_dir = Path(directory)
        tar_dir = temp_dir / "tar"
        input_dir = tar_dir / "input_files"
        # make needed directories
        input_dir.mkdir(parents=True, exist_ok=True)
        # dealing with convergence
        if 'convergence' not in self.properties:
            code_inputs.write_inputs(input_dir)
            errors.append(code_inputs.error)
        else:
            for conv_key, conv_values in self.properties['convergence'].items():
                key_dir = input_dir / conv_key
                for conv_value in conv_values:
                    value_dir = key_dir / str(conv_value)
                    value_dir.mkdir(parents=True, exist_ok=True)
                    code_inputs.set_control(conv_key, conv_value)
                    code_inputs.write_inputs(value_dir)
                    errors.append(code_inputs.error)
        if all([error is None for error in errors]):
            shutil.make_archive((tar_dir / "input_files").as_posix(), "tar",
                                root_dir=input_dir, base_dir=None, verbose=True)
            return tar_dir, "input_files.tar", code_inputs.info
        else:
            print(f'error: {code_inputs.error}')
            return None, "FileNotFoundError", {}








