""" The CIF reader and preprocessor (uses gemmi package)
"""
import re
from io import TextIOWrapper, BufferedReader

import numpy as np
from ase.data import chemical_symbols
from gemmi.cif import read_string, as_number, as_string
from werkzeug.datastructures import FileStorage

from gims.data import hm_groups


__all__ = ['CifPreprocessor', ]


class CifPreprocessor:
    def __init__(self, cif_file):
        """ A class that reads and preprocesses cif files
        Args:
            cif_file: a file name or object
        """

        self._block = None
        self.lines = None
        self.symbols = None
        self.coords = None
        self.cell_pars = None
        self.space_group = None

        if isinstance(cif_file, TextIOWrapper):  # file object got with open
            self.lines = cif_file.read()
        elif isinstance(cif_file, (BufferedReader, FileStorage)):  # 1 - file object got with open(rb0
            self.lines = cif_file.read().decode('utf-8')
        else:
            with open(cif_file) as f:
                self.lines = f.read()

        cif = read_string(self.lines)
        try:
            # stands almost for every case
            self._block = cif.sole_block()
        except RuntimeError:
            # Springer Materials
            for block in cif:
                if 'standardized_unitcell' in block.name:
                    self._block = block
                    break
        except IndexError:
            raise ValueError('Did not find block with data in the given CIF file')
        if self._block is None:
            raise ValueError('Did not find block with data in the given CIF file')

    def save(self, save_name):
        """ Checks cif file for errors and saves it to a file
        Args:
            save_name: a name of the file to save cif to
        """
        self._parse()
        # if everything is fine, then go ahead with saving
        hm_re = re.compile(r"_symmetry_space_group_name_H-M +'?(.*)\b")
        hm_match = hm_re.search(self.lines)
        if hm_match is not None:
            given_hm = hm_match.group(1)
            hm = re.sub(r"[()_ ]", r"", given_hm)
            if hm in hm_groups:
                print(given_hm, hm_groups[hm])
                self.lines = self.lines.replace(given_hm, hm_groups[hm])
        with open(save_name, 'w') as f:
            f.write(self.lines)

    def _parse(self):
        """ Checks CIF block for errors, and parses it
        """
        # trying to parse the file and return meaningful error in case of failure
        # cell key-value pairs
        cell_kwds = ['length_a', 'length_b', 'length_c', 'angle_alpha', 'angle_beta', 'angle_gamma']
        # symmetry key-value pairs
        symm_kwds = ['space_group_name_h-m', 'int_tables_number']
        sg_kwds = ['group_name_h-m_alt', 'it_number']
        # given in a loop
        atom_site_kwds = ['label', 'type_symbol', '?wyckoff_symbol', '?symmetry_multiplicity', '?wyckoff_label',
                          '?fract_x', '?cartn_x', '?fract_y', '?cartn_y', '?fract_z', '?cartn_z']

        try:
            self.cell_pars = [as_number(self._block.find_value(f'_cell_{kwd}')) for kwd in cell_kwds]
        except TypeError as e:
            raise ValueError('Some cell parameters are missing in the given CIF file') from e

        symm_pars = [self._block.find_value(f'_symmetry_{kwd}') for kwd in symm_kwds]
        sg_pars = [self._block.find_value(f'_space_group_{kwd}') for kwd in sg_kwds]

        if all([x is None for x in symm_pars + sg_pars]):
            raise ValueError('Symmetry information is missing from the given CIF file')

        atoms_table = self._block.find('_atom_site_', atom_site_kwds)
        existing_site_kwds = [kwd[1:] if kwd.startswith('?') else kwd for i, kwd in enumerate(atom_site_kwds)
                              if atoms_table.has_column(i)]

        is_fractional = sum(['fract' in x for x in existing_site_kwds]) == 3
        is_cartesian = sum(['cartn' in x for x in existing_site_kwds]) == 3

        if is_fractional == is_cartesian:
            raise ValueError('A mix of fractional and cartesian coordinates in CIF file is not implemented')
        if is_cartesian:
            raise ValueError('Cartesian coordinates in CIF files are not yet implemented')

        try:
            labels_idx = existing_site_kwds.index('label')
            symbols_idx = existing_site_kwds.index('type_symbol')
        except ValueError as e:
            raise ValueError('No atomic labels or types found in the given CIF file') from e

        atoms_table = self._block.find('_atom_site_', existing_site_kwds)

        self.symbols = _parse_atom_symbols([as_string(x) for x in atoms_table.column(labels_idx)],
                                      [as_string(x) for x in atoms_table.column(symbols_idx)])
        self.space_group = _parse_symmetries(symm_pars, sg_pars)
        # change setting! (maybe employ the code from ASE?)
        coords = [[as_number(x) for x in atoms_table.column(i)]
                  for i, kwd in enumerate(existing_site_kwds) if 'fract' in kwd or 'cartn' in kwd
                  ]
        self.coords = np.array(coords).T


def _parse_atom_symbols(labels, types):
    # find atomic symbols (won't work for something like 'boron', which is valid, but I have not met
    # such a type)
    types_symbols = [re.findall('[A-Z][a-z]?', t) for t in types]
    # check for partial occupancies
    if any([len(s) > 1 for s in types_symbols]):
        raise ValueError('Partial occupancies met in the given CIF file')
    # check for atomic types to be present in the Periodic table
    if any([len(s) < 1 for s in types_symbols]) or any([any([s_i not in chemical_symbols for s_i in s])
                                                        for s in types_symbols]):
        raise ValueError('Unknown atomic types met in the given CIF file')
    # check for labels to be consistent with types (should give a warning though)
    types_symbols = [s_i for s in types_symbols for s_i in s]
    if len(labels) != len(types_symbols):
        raise ValueError('The number of labels differs with the number of types in the given CIF file')
    return types_symbols


def _parse_symmetries(symmetry_params, sg_params):
    # returns the space group number if it's given
    sg, sg_alt = symmetry_params[1], sg_params[1]
    if sg is not None and sg_alt is not None and sg != sg_alt:
        raise ValueError('Inconsistent space group numbers given in a CIF file')
    sg = sg if sg is not None else sg_alt
    if sg is not None:
        return as_number(sg)
    # if space group is not given, then check the H-M notation
    hm, hm_alt = symmetry_params[0], sg_params[0]
    if hm is not None and hm_alt is not None and hm != hm_alt:
        raise ValueError('Inconsistent Hermann-Mauguin groups given in a CIF file')
    hm = as_string(hm) if hm is not None else as_string(hm_alt)
    # check the H-M notation
    if hm.replace(" ", "") in hm_groups:
        return hm_groups[hm.replace(" ", "")]
    return hm
