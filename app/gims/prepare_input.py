import os
import re
import shutil
import json
import tempfile
from pathlib import Path
from werkzeug.utils import secure_filename
from flask import send_from_directory

from gims import inputs, constants
from gims.cif import CifPreprocessor
from gims.structure import Structure, read


AIMS_NAMES = ['*.in', 'geometry.in.next_step']


def write_input_files(code_name, data, tmp_dir, species_dir, session=None):
    """
    Writes the prepared input file to temp directory.

    Parameters:

    code_name: String
        Name of the code for which to prepare the input files.

    data: Dictionary
        Numerical Parameters need for the creation of the input files

    tmp_dir: TemporaryDirectory object
        Temporary directory to write the input file into and archive them as tar-ball.

    species_dir: String (Path)
        Path to the root directory for the basis set data for each species.
    """
    species_dir = Path(species_dir) / code_name
    tar_dir = Path(tmp_dir) / "tar"
    input_dir = tar_dir / "input_files"
    # make needed directories
    input_dir.mkdir(parents=True, exist_ok=True)
    code = inputs.inputs_cls(code_name)(data, species_dir, session)
    code.write_inputs(input_dir)
    if code.error is None:
        shutil.make_archive(os.path.join(tmp_dir, "input_files"), "tar", tar_dir)
        return tmp_dir, "input_files.tar", code.info
    else:
        return None, "FileNotFoundError", {}


def get_input_files(data_json, species_dir, session):
    """Generate the control.in file from the ASE calculator object.

    Parameters:

    data_json: JSON dictionary with all needed parameters to prepare the input files. This includes
        numerical parameters, name of the code, structure data in json format (optional)

    species_dir: String (Path)
        path to the species root directory.
    """

    data = json.loads(data_json)
    # print(data)
    code_name = data["code"]
    with tempfile.TemporaryDirectory() as temp_dir:
        tar_dir, tar_file, dummy = write_input_files(
            code_name, data, temp_dir, species_dir, session
        )

        if tar_file == "FileNotFoundError":
            response = "FileNotFoundError"
        else:
            print("I am sending the tar")
            response = send_from_directory(tar_dir, tar_file, as_attachment=True)

    return response


def get_download_info(data_json, species_dir, session=None):
    """Prepare download info before actual download is requested.

    Currently, this only is used in the context of the band structure workflow.

    Args:
        data_json: JSON object
            Dictionary including structural data and supportive information.
        species_dir: String (Path)
            path to the species root directory.
        session: (dict, NoneType)
            a session-specific dictionary
    """
    data = json.loads(data_json)
    # print(data)
    code_name = data["code"]
    with tempfile.TemporaryDirectory() as temp_dir:

        tar_dir, tar_file, download_info = write_input_files(
            code_name, data, temp_dir, species_dir, session
        )
        print(f"Response: {download_info}")

        if tar_file == "FileNotFoundError":
            response = "FileNotFoundError"
        else:
            response = json.dumps(download_info)
    return response


def generate_slab(data_json, session):
    """Generates a slab (as of now, with ASE) using structure in JSON"""
    from .slab import Slab
    slab = Slab.from_json(data_json)
    slab.get_slab()
    return slab.get_json(session)


def terminate_slab(data_json, session):
    """Terminates a slab trimming unnecessary atomic layers"""
    from .slab import Slab
    slab = Slab.from_json(data_json)
    slab.terminate()
    return slab.get_json(session)


# def generate_k_mesh(data_json):
#     """Terminates a slab trimming unnecessary atomic layers"""
#     from .k_mesh import KMesh
#     k_mesh = KMesh(data_json)
#     return k_mesh.get_points(to_json=True)


def primitive_cell(data, session):
    """Prepare primitive cell from structure data in json format.

    This function uses spglib to generate the primitive cell.

    Parameters:

    data: dict
        Dictionary containing the structure data.
    session: dict
        Dictionary with session-specific parameters
    """
    # print(data)
    atoms = Structure.from_dict(data)
    sym_thresh = session.get("symThresh", constants.sym_thresh)
    primitive = atoms.get_primitive_cell(sym_thresh)
    f_name = data["fileName"] + " (primitive)"
    # print(primitive)
    if len(primitive) == len(atoms):
        response = "ErrorIsAlreadyPrimitive"
    else:
        response = primitive.to_json(f_name, sym_thresh)

    return response


def conventional_cell(data, session):
    """Prepare conventional cell from structure data in json format.

    Parameters:

    data: dict
        Dictionary containing the structure data.
    session: dict
        Dictionary with session-specific parameters
    """
    # print(data)
    atoms = Structure.from_dict(data)
    sym_thresh = session.get("symThresh", constants.sym_thresh)
    conventional = atoms.get_conventional_cell(sym_thresh)
    f_name = data["fileName"] + " (conventional)"
    # print(primitive)
    if len(conventional) == len(atoms):
        response = "ErrorIsAlreadyConventional"
    else:
        response = conventional.to_json(f_name, sym_thresh)

    return response


def structure_auto_detection(file_name, session):
    """Wrapper for the ASE auto-detection of the input files.
    Args:
        file_name (string): the name of the file to read
        session (dict): a dictionary of Flask session parameters
    """

    try:
        # The format is specified by the file extension.
        file_format = 'aims' if any([Path(file_name).match(p) for p in AIMS_NAMES]) else None
        structure = read(file_name, format=file_format)
        assert len(structure.get_positions()) > 0
    except KeyError:
        # Could not guess the proper file format.
        return "Could not properly read the structure file"
    except (Exception,) as e:
        return f"ErrorParsingGeometryFile: {str(e)}"
        # print('Something else went wrong. Please excuse!')
    else:
        sym_thresh = session.get("symThresh", constants.sym_thresh)
        json_structure = structure.to_json(file_name, sym_thresh)
    return json_structure


def get_json_structure(files, session):
    """Read the uploaded structure file(s) and prepare json file, which the client
    understands.
    """
    f = files["file"]
    with tempfile.TemporaryDirectory() as temp_dir:
        save_name = os.path.join(temp_dir, secure_filename(f.filename))
        if f.filename.lower().endswith('cif'):
            # pre-handle
            try:
                cif = CifPreprocessor(cif_file=f)
                cif.save(save_name)
            except (Exception, ) as e:
                return f"ErrorParsingGeometryFile: {str(e)}"
        else:
            f.save(save_name)
        return structure_auto_detection(save_name, session)


def update_json_structure(data, session):
    """Update the supportive structure information

    Parameters:

    data: dict
        Dictionary containing the structure data.
    session: dict
        Dictionary with session-specific parameters
    """

    json_structure = {}
    try:
        atoms = Structure.from_dict(data)
        sym_thresh = session.get("symThresh", constants.sym_thresh)
        json_structure = atoms.to_json("", sym_thresh=sym_thresh)
    except (Exception,):
        pass
    return json_structure
