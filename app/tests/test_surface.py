"""A test suite for slab creation
"""
from pathlib import Path
from gims.structure import read
from gims.slab import Slab


def test_slab_111_creation():
    """ Tests slab creation with (111) Miller indices
    """
    tests_dir = Path(__file__).parent
    file_name = (tests_dir / 'Structures' / 'geometry.in').as_posix()
    atoms = read(file_name)
    slab = Slab(atoms)
    slab.get_slab(indices=(1, 1, 1), layers=10, vacuum=12)
    assert slab.number_of_atomic_layers == 10
    assert len(slab.atoms) == 40
