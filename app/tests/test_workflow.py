"""A set of tests for Workflow backend implementation"""

import json
import tempfile
from pathlib import Path

from gims.workflow import Workflow


def test_workflow():
    tests_dir = Path(__file__).parent
    file_name = (tests_dir / 'FHIaims' / 'test_files' / 'workflow.json').as_posix()
    with open(file_name, 'r') as f:
        wf_json = json.loads(f.read())
    workflow = Workflow(wf_json)
    with tempfile.TemporaryDirectory() as temp_dir:
        workflow.run(temp_dir)
