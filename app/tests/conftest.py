""" Pytest fixtures for the GIMS app
"""
import json

import pytest

from ase.io import read

from gims import app as gims_app


@pytest.fixture()
def gims():
    app = gims_app
    app.config.update({
        "TESTING": True,
    })
    yield app


@pytest.fixture()
def client(gims):
    return gims.test_client()


@pytest.fixture()
def structure_json():
    def _from_file(file_name, sym_thresh=None):
        atoms = read(file_name)
        atoms_json = [
            {'position': atom.position.tolist(),
             'species': atom.symbol,
             'initMoment': atom.magmom,
             'constraint': i in atoms.constraints,
             'charge': atom.charge} for i, atom in enumerate(atoms)]
        struct_json = {
            'cell': atoms.cell.array.tolist(),
            'positions': atoms_json,
            'fileName': file_name.as_posix()
        }
        if sym_thresh is not None:
            struct_json['symThresh'] = sym_thresh
        return json.dumps(struct_json)
    return _from_file


@pytest.fixture()
def control_json():
    return json.dumps({
        "code": "FHIaims",
        "form": {
            "species": ["Si"],
            "xc": "pbe",
            "basisSettings": "light",
            "k_grid": [5, 5, 5]
        }
    })
