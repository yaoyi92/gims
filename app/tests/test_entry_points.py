""" A test suite for GIMS entry points (defined in __init__.py)
"""
import json
import pytest
import tarfile
from io import BytesIO
from pathlib import Path

import numpy as np
from flask import session

expected_control_in = """\
xc                                 pbe
k_grid                             5 5 5
relativistic                       atomic_zora scalar"""


def test_index(client):
    response = client.get('/')
    assert response.status_code == 302
    assert b"/static/index.html" in response.data


@pytest.mark.parametrize("file_name, expected",
                         [("geometry.in", {"lattice": [[7.55, 0.0, 0.0], [0.0, 7.55, 0.0], [0.0, 0.0, 7.55]],
                                           "spacegroup": 216}),
                          ("625234.cif",  {"lattice": [[17.607, 0.0, 0.0], [0.0, 8.835, 0.0], [-4.961, 0.0, 21.818]],
                                           "spacegroup": 14}),
                          ("geometry.in.next_step", {"lattice": [[10.0, 0.0, 0.0], [0.0, 10.0, 0.0], [0.0, 0.0, 10.0]],
                                                     "spacegroup": 221})
                          ])
def test_parse_geometry_file(client, file_name, expected):
    geom_file = Path(__file__).parent / 'Structures' / file_name
    response = client.post('/parse-geometry-file', data={
        "file": (geom_file.open("rb"), file_name)
    })
    assert response.status_code == 200
    data = json.loads(response.data)
    assert np.allclose(data['lattice'], expected['lattice'], atol=1.e-3)
    assert data['structureInfo']['spacegroup']['value'] == expected['spacegroup']


def test_update_structure_info(client, structure_json):
    file_name = 'geometry.in'
    geom_file = Path(__file__).parent / 'Structures' / file_name
    response = client.post('/update-structure-info', data=structure_json(geom_file))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data['lattice'] == [[7.55, 0.0, 0.0], [0.0, 7.55, 0.0], [0.0, 0.0, 7.55]]
    assert data['structureInfo']['spacegroup']['value'] == 216


def test_generate_control_in(client, control_json):
    response = client.post('/generate-control-in', data=control_json)
    assert response.status_code == 200
    tar_obj = BytesIO(response.data)
    with tarfile.open(fileobj=tar_obj) as f:
        name = './input_files/control.in'
        file_content = f.extractfile(name).read().decode('utf-8')
        assert expected_control_in in file_content


def test_get_download_info(client, control_json):
    response = client.post('/get-download-info', data=control_json)
    assert response.status_code == 200
    #  I really do not understand why it's needed
    download_info = json.loads(response.data)
    assert 'references' in download_info
    assert len(download_info['references']) == 0


def test_get_primitive_cell(client, structure_json):
    struct_file_name = 'geometry.in'
    geom_file = Path(__file__).parent / 'Structures' / struct_file_name
    response = client.post('/get-standard-cell', data=structure_json(geom_file))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data['lattice'] == [[0.0, 3.775, 3.775], [3.775, 0.0, 3.775], [3.775, 3.775, 0.0]]
    with client:
        client.post('/get-standard-cell', data=structure_json(geom_file, sym_thresh=2.e-4))
        assert session['symThresh'] == 2.e-4


def test_upload_workflow(client):
    wf_file_name = 'workflows.tar.gz'
    workflow_file = Path(__file__).parent / 'FHIaims' / 'test_files' / wf_file_name
    response = client.post('/upload-workflow', data=workflow_file.open('rb').read())
    assert response.status_code == 200
    file_names = [f['name'] for f in json.loads(response.data)]
    assert "workflows/DFT/band1001.out" in file_names


def test_get_bz_vertices(client):
    response = client.post('/get-bz-vertices', data='[[7.55, 0.0, 0.0], [0.0, 7.55, 0.0], [0.0, 0.0, 7.55]]')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data['path'][0][0] == ['G', 'X', 'M', 'G', 'R', 'X']


def test_get_available_bases(client):
    response = client.get('/get-bases')
    assert response.status_code == 400
    response = client.get('/get-bases?code=someCode')
    assert response.status_code == 400
    response = client.get('/get-bases?code=FHIaims')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert all([x in data for x in ['light', 'intermediate', 'tight']])


def test_get_slab(client, structure_json):
    struct_file_name = 'geometry.in'
    geom_file = Path(__file__).parent / 'Structures' / struct_file_name
    struct = json.loads(structure_json(geom_file))
    response = client.post('/get-slab', data=json.dumps(struct))
    assert response.status_code == 400
    struct.update({
        "slab_data": {
            "miller": [1, 0, 0],
            "layers": 10,
            "vacuum": 12
        }
    })
    response = client.post('/get-slab', data=json.dumps(struct))
    assert response.status_code == 200
    slab = json.loads(response.data)
    assert len(slab['atoms']) == 30
    expected_cell = np.array([[7.55, 0.0, 0.0], [0.0, 7.55, 0.0], [0.0, 0.0, 28.9875]])
    assert np.allclose(np.array(slab['lattice']), expected_cell)


def test_terminate_slab(client, structure_json):
    struct_file_name = 'slab_geometry.in'
    geom_file = Path(__file__).parent / 'Structures' / struct_file_name
    struct = json.loads(structure_json(geom_file))
    struct.update({
        "slab_data": {
            "terminations": [1, 7],
        }
    })
    response = client.post('/terminate-slab', data=json.dumps(struct))
    assert response.status_code == 200
    slab = json.loads(response.data)
    assert len(slab['atoms']) == 28


