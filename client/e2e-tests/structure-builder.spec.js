// @ts-check
const { test, expect } = require('@playwright/test');

test.describe.configure({ mode: 'parallel' });

test.beforeEach(async ({ page }) => {
  await page.goto('https://gims.ms1p.org/static/index.html');
  // await page.goto('http://127.0.0.1:5000/static/index.html');
  await page.locator('#SimpleCalculation-workflow-button').click();
});

test.describe('Test Structure Builder', () => {
  test('with the default structure', async ({page}) => {

    const structTable = page.locator('div.StructureInfo > table tr')
    await expect(structTable).toHaveCount(10)

    const structData = await getStructData(page)
    expect(structData['number']).toEqual("12")
    expect(structData['chemical']).toEqual("Cd4Sr4Ta4")
    expect(structData['lattice']).toEqual("7.55, 7.55, 7.55, 90, 90, 90")
    expect(structData['bravais']).toContain("primitive cubic")
    expect(structData['symmetry']).toEqual("0.00001")
    expect(structData['spacegroup']).toEqual("216")
    expect(structData['hall']).toEqual("F -4 2 3")
  })

  test('should change lattice vector', async ({page}) => {
    const lattice = page.locator('div.lattice-vector').filter({has_text: 'a'}).getByRole('textbox').nth(0)
    await lattice.click()
    await expect(lattice).toBeFocused()
    await lattice.fill('7')
    await lattice.press('Enter')
    await page.waitForLoadState('networkidle')
    await expect(lattice).toHaveValue("7.000000")
    // todo - StructureInfo closes over here
    await page.getByText('Structure Info').click();

    const structData = await getStructData(page)
    expect(structData['lattice']).toEqual("7, 7.55, 7.55, 90, 90, 90")
    expect(structData['bravais']).toContain("primitive tetragonal")
    expect(structData['spacegroup']).toEqual("119")
    expect(structData['hall']).toEqual("I -4 -2")
  })

  test('should remove lattice vectors', async ({page}) => {
    await page.locator("#LatticeVectors").getByRole('img').click()
    await page.waitForLoadState('networkidle')
    await page.getByText('Structure Info').click()

    const structTable = page.locator('div.StructureInfo > table tr')
    await expect(structTable).toHaveCount(2)
  })

  test('should add lattice vectors back', async ({page}) => {
    await page.locator("#LatticeVectors").getByRole('img').click()
    await page.waitForLoadState('networkidle')
    await page.getByRole('button', {name: 'Add lattice vectors'}).click()
    const lattice = page.locator('div.lattice-vector').filter({has_text: 'a'}).getByRole('textbox').nth(0)
    await expect(lattice).toHaveValue("10.000000")
    await page.getByText('Structure Info').click()

    const structData = await getStructData(page)
    expect(structData['lattice']).toEqual("10, 10, 10, 90, 90, 90")
    expect(structData['spacegroup']).toEqual("160")
    expect(structData['hall']).toEqual('R 3 -2"')  // why are the quotes there?
  })

  test('should make supercell', async ({page}) => {
    await page.getByText('Supercell', { exact: true }).click();
    await page.locator('#Supercell').getByRole('textbox').first().fill('2');
    await page.locator('#Supercell').getByRole('textbox').nth(1).fill('2');
    await page.locator('#Supercell').getByRole('textbox').nth(2).fill('2');
    await page.getByRole('button', { name: 'Create Supercell' }).click();
    await page.getByText('Structure Info').click()
    const structData = await getStructData(page)
    expect(structData['number']).toEqual("96")
    expect(structData['lattice']).toEqual("15.1, 15.1, 15.1, 90, 90, 90")
    expect(structData['spacegroup']).toEqual("216")
  })

  test('should constuct primitive cell', async ({page}) => {
    await page.getByText('Standardized Cells').click();
    await page.getByText('Primitive Cell', { exact: true }).click()
    await page.getByText('Structure Info').click()
    const structData = await getStructData(page)
    expect(structData['number']).toEqual("3")
    expect(structData['chemical']).toEqual("CdSrTa")
    expect(structData['bravais']).toContain("face-centred cubic")
    expect(structData['is']).toEqual("true")
  })

  test('should make a slab', async ({page}) => {
    await page.getByText('Surface (Slab) Construction').click();
    await expect(page.locator('#top-layer')).not.toBeVisible()
    await expect(page.locator('#bottom-layer')).not.toBeVisible()
    const surfaceInputs = page.locator('#Surface').getByRole('textbox')
    await surfaceInputs.first().fill('1');
    await surfaceInputs.nth(1).fill('1');
    await surfaceInputs.nth(2).fill('1');
    await surfaceInputs.nth(3).fill('12');
    await surfaceInputs.nth(4).fill('12');
    await page.getByRole('button', { name: 'Create Slab' }).click();
    await page.getByText('Structure Info').click()
    const structData = await getStructData(page)
    expect(structData['number']).toEqual("44")
    expect(structData['chemical']).toEqual("Cd16Sr12Ta16")
    expect(structData['bravais']).toContain("primitive hexagonal")
    expect(structData['spacegroup']).toEqual("156")
    // check slab termination
  })

  test('should terminate a slab', async ({page}) => {
    await page.getByText('Surface (Slab) Construction').click();
    const surfaceInputs = page.locator('#Surface').getByRole('textbox')
    const inputs = [1, 1, 1, 12, 12]
    for (let i=0; i < inputs.length; i++)
      await surfaceInputs.nth(i).fill(inputs[i].toString());
    await page.getByRole('button', { name: 'Create Slab' }).click()
    await expect(page.locator('#top-layer')).toBeVisible()
    await expect(page.locator('#bottom-layer')).toBeVisible()
    await page.locator('#top-layer').selectOption('8');
    await page.locator('#bottom-layer').selectOption('2');
    await page.getByRole('button', { name: 'Terminate Slab' }).click()
    await page.getByText('Structure Info').click()
    const structData = await getStructData(page)
    expect(structData['number']).toEqual("28")
    expect(structData['chemical']).toEqual("Cd8Sr12Ta8")
  })
})

async function getStructData(page) {
      return Object.fromEntries(await page.$$eval('div.StructureInfo > table tr', (rows) => {
      return rows.map(row => {
        const name = row.querySelector('td:nth-child(1)').textContent.trim();
        const value = row.querySelector('td:nth-child(2)').textContent.trim();
        return [name.split(' ')[0].toLowerCase(), value]
      });
    }));

}