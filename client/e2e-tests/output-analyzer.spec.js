// @ts-check
const fs = require('fs');
const path = require('path');
const { test, expect } = require('@playwright/test');

test.describe.configure({ mode: 'parallel' });

test.beforeEach(async ({ page }) => {
  // await page.goto('http://127.0.0.1:5000/static/index.html');
  await page.goto('https://gims.ms1p.org/static/index.html');
  await page.getByText('Output Analyzer').click();
});


test.describe('Test Output Analyzer', () => {
  test('hirshfeld and mulliken analysis', async ({page}) => {
    await uploadFiles(page, 'e2e-tests/data/hirshfeld/')
    await expect(page.getByText('Hirshfeld')).toBeVisible()
    await expect(page.getByText('Mulliken')).toBeVisible()
    const results = await getResultsInfo(page)
    expect(results['total']).toEqual(-2712156.98972)
    expect(results['fermi']).toEqual(-5.32947)
    const calcInfo = await getCalculationInfo(page)
    expect(calcInfo['version']).toEqual(221103)
    expect(calcInfo['commit']).toEqual('fe4ac0929')
  });

  test('band structure', async ({page}) => {
    await uploadFiles(page, 'e2e-tests/data/band_structure/')
    await expect(page.getByText('Hirshfeld')).not.toBeVisible()
    await expect(page.getByText('Mulliken')).not.toBeVisible()
    const results = await getResultsInfo(page)
    expect(results['total']).toEqual(-2714235.72891)
    expect(results['fermi']).toEqual(-5.16675)
    const calcInfo = await getCalculationInfo(page)
    expect(calcInfo['version']).toEqual(220915)
    expect(calcInfo['commit']).toEqual('4cad2e13b')
    await expect(page.getByText('Select band segment')).toBeVisible()

  });
})

test.describe('Debug test', () => {
  test('mbd-h2-dimer', async ({page}) => {
    await uploadFiles(page, 'e2e-tests/data/mbd-h2-dimer/')
    await expect(page.getByText('Hirshfeld')).toBeVisible()
    const results = await getResultsInfo(page)
    expect(results['total']).toEqual(-2714235.72891)
    expect(results['fermi']).toEqual(-5.16675)
    const calcInfo = await getCalculationInfo(page)
    expect(calcInfo['version']).toEqual(220915)
    expect(calcInfo['commit']).toEqual('4cad2e13b')
    await expect(page.getByText('Select band segment')).toBeVisible()

  });
})

/** Uploads a file (or files) to OutputAnalyzer
 *
 * @param {import('playwright/test').Page} page a Page fixture of Playwright
 * @param {string} name a name of the file (or dir) to upload
 * @param isDir a flag showing if a directory is uploaded
 * @returns {Promise<void>}
 */
async function uploadFiles(page, name, isDir=true) {
  const fileChooserPromise = page.waitForEvent('filechooser')
  let fileNames
  if (isDir) {
    await page.getByLabel('Select a folder containing the files').check()
    fileNames = fs.readdirSync(name).map(f => path.join(name, f))
  } else fileNames = name

  await page.getByRole('textbox').click()
  const fileChooser = await fileChooserPromise
  await fileChooser.setFiles(fileNames)
}

async function getResultsInfo(page) {
  return Object.fromEntries(await page.$$eval('table.results-info-fields tr', (rows) => {
    return rows.map(row => {
      const fieldNames = {
        total: 'total',
        fermi: 'fermi',
        highest: 'homo',
        lowest: 'lumo',
        estimated: 'gap',
        cell: 'volume'
        }
      const name = row.querySelector('td:nth-child(1)').textContent.trim().split(' ')[0].toLowerCase();
      const value = row.querySelector('td:nth-child(2)').textContent.trim();
      return [fieldNames[name], +value]
    });
  }));
}

async function getCalculationInfo(page) {
  return Object.fromEntries(await page.$$eval('table.calculation-info-fields tr', (rows) => {
    return rows.map(row => {
      const fieldNames = {
        code: 'version',
        commit: 'commit',
        number: 'n_tasks',
        total: 'time',
        peak: 'memory',
        largest: 'array_mem',
        calculation: 'exit'
        }
      const name = row.querySelector('td:nth-child(1)').textContent.trim().split(' ')[0].toLowerCase();
      const value = row.querySelector('td:nth-child(2)').textContent.trim();
      return [fieldNames[name], ['commit', 'calculation'].indexOf(name) >= 0 ? value : +value]
    });
  }));
}