import OutputExciting from "../../../output-analyzer-mod/OutputExciting.js"
import * as fs from 'fs'

const FILES_FOLDER = 'src/__tests__/Exciting/bandstructure/'
const TEST_OUTPUT_FILE_ENDING = '.test_output.json'

const FILE_NAMES = [
  'INFO.OUT',
  'bandstructure.xml',
  'dos.xml',
  'input.xml',
]

const fileObjectArray = []  // Prepare the array of files for parseFiles()
FILE_NAMES.forEach( fileName => {
  let content = fs.readFileSync(FILES_FOLDER+fileName, "utf8")
  fileObjectArray.push( {name: fileName, content: content} )
})


const scfLoopsTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+FILE_NAMES[0]+TEST_OUTPUT_FILE_ENDING, "utf8"))
const dosTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+FILE_NAMES[2]+TEST_OUTPUT_FILE_ENDING, "utf8"))
const bsInfoTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+FILE_NAMES[1]+TEST_OUTPUT_FILE_ENDING, "utf8"))

let outputInstance = new OutputExciting()

test('Parse files', () => {
	outputInstance.parseFiles(fileObjectArray)
	expect(getParsedFiles(outputInstance)).toEqual(FILE_NAMES)
})

test('Parse output file', () => {
  outputInstance.parseOutputFile()
  // test output file generation -> fs.writeFile(FILES_FOLDER+FILE_NAMES[0]+TEST_OUTPUT_FILE_ENDING, JSON.stringify(outputInstance.scfLoops), () => {})
  testScfLoopData(outputInstance, scfLoopsTestOutput)
})

test('DOS data', () => {
  let dosData = outputInstance.getFirstFileDosData()
  //test output file generation -> fs.writeFile(FILES_FOLDER+FILE_NAMES[2]+TEST_OUTPUT_FILE_ENDING, JSON.stringify(dosData),() => {})
  expect(dosData).toEqual(dosTestOutput)
})

test('Band structure data', () => {
  let bsInfo = outputInstance.getBsInfo()
  // test output file generation -> fs.writeFile(FILES_FOLDER+FILE_NAMES[1]+TEST_OUTPUT_FILE_ENDING, JSON.stringify(outputInstance.getBsInfo()),() => {})
  expect(bsInfo).toEqual(bsInfoTestOutput)
})


function getParsedFiles(output){
  const files = []
  output.files.output.forEach( (c, fileName) => files.push(fileName) )
  if (output.bsData) files.push('bandstructure.xml')
  output.files.dos.forEach( (c, fileName) => files.push(fileName) )
  output.files.input.forEach( (c, fileName) => files.push(fileName) )
  return files
}


function testScfLoopData(output, scfLoopsTestOutput){

  const scfLoopsTestInput = output.scfLoops

  for (var i = 0; i < scfLoopsTestInput.length; i++) {
    const loopData = scfLoopsTestInput[i]
    for (const atom of loopData.structure.atoms) delete atom.radius
    expect(loopData.finalScfEnergies).toEqual(scfLoopsTestOutput[i].finalScfEnergies)
    expect(loopData.structure).toEqual(scfLoopsTestOutput[i].structure)
    expect(loopData.iterations).toEqual(scfLoopsTestOutput[i].iterations)
    expect(loopData.isConverged).toEqual(scfLoopsTestOutput[i].isConverged)
  }
}
