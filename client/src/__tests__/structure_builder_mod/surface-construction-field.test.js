/**
 * @author Andrey Sobolev
 *
 * @fileOverview tests for SurfaceConstructionField module
 */

import SurfaceConstructionField from "../../structure-builder-mod/SurfaceConstructionField";
let field = new SurfaceConstructionField()

describe('Surface construction Field', () => {
  beforeEach(() => {
    document.body.appendChild(field.e)
  })

  test('shows termination elements', () => {
    let terminationDiv = document.querySelector('div.termination-div')
    field.showTermination('geometry.in')
    expect(terminationDiv).not.toBeVisible()
    field.showTermination('slab.in')
    expect(terminationDiv).toBeVisible()
  })

  test('returns slab creation data', () => {
    let millerIdx = document.querySelectorAll('input.miller')
    let nLayers = document.querySelector('input.number-of-layers')
    let vacuum = document.querySelector('input.vacuum-layer')
    millerIdx.forEach(f => f.value = '1')
    nLayers.value = '10'
    vacuum.value = '12'
    expect(field.getCreationValues()).toEqual({
      miller: [1, 1, 1],
      layers: 10,
      vacuum: 12
    })
    millerIdx[0].value = ''
    expect(field.getCreationValues()).toEqual({
      miller: [0, 1, 1],
      layers: 10,
      vacuum: 12
    })
    field.clearValues()
    expect(field.getCreationValues()).toEqual({
      miller: [0, 0, 0],
      layers: 0,
      vacuum: 0
    })
  })

  test('populates termination boxes', () => {
    let topLayer = document.querySelector('#top-layer')
    let bottomLayer = document.querySelector('#bottom-layer')
    const terminations = [['A', 0], ['B', 1], ['A', 0], ['B', 1], ['A', 0]]
    field.populateTerminationBoxes(terminations)
    expect([...topLayer.options].map(option => option.text)).toEqual(['A', 'B'])
    expect([...bottomLayer.options].map(option => option.text)).toEqual(['A', 'B'])
    expect(topLayer.value).toEqual('4')
    expect(bottomLayer.value).toEqual('0')
    expect(field.getTermValues()).toEqual(['0', '4'])
    field.clearValues()
    expect([...topLayer.options].map(option => option.text)).toEqual([])
  })
})
