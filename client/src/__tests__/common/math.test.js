/**
 * @author Andrey Sobolev
 *
 * @fileOverview tests for math library
 */

import * as math from "../../common/math";

describe('Math functions', () => {

  test('should add vectors', () => {
    const a = [1., 2., 3.]
    const b = [4., 5., 6.]
    expect(math.add(a, b)).toEqual([5., 7., 9.])
  })

  test('should add up one vector to another', () => {
    const a = [1., 2., 3.]
    const b = [4., 5., 6.]
    expect(math.addUp(a, b)).toEqual([5., 7., 9.])
    expect(a).toEqual([5., 7., 9.])
  })

  test('should subtract vectors', () => {
    const a = [1., 2., 3.]
    const b = [4., 5., 6.]
    expect(math.subtract(b, a)).toEqual([3., 3., 3.])
  })

  test('should multiply vector by scalar', () => {
    const a = [1., 2., 3.]
    expect(math.multiplyScalar(a, 3)).toEqual([3., 6., 9.])
  })

  test('should divide vector by scalar', () => {
    const a = [4., 2., 0.]
    expect(math.divideScalar(a, 2)).toEqual([2., 1., 0.])
  })

  test('should get distance between two points', () => {
    const a = [3., 0., 0.]
    const b = [0., 4., 0.]
    expect(math.getDistance(a, b)).toEqual(5.)
  })

  test('should get angle made of three points', () => {
    const a = [3., 0., 0.]
    const b = [0., 0., 0.]
    const c = [0., 4., 0.]
    expect(math.getAngle(a, b, c)).toEqual(90.)
  })

  test('should get dihedral angle from four points', () => {
    const a = [0., 1., 0.]
    const b = [1., 0., 0.]
    const c = [0.5, 0.5, 0.5]
    const d = [0., 0., 0.]
    expect(math.getTorsionAngle(a, b, c, d)).toBeCloseTo(60., 8)
  })

  test('should get min of the matrix along the axis', () => {
    const a = [[3., 0., 0.],
      [4., 5., 6.],
      [7., 3., 4.]]
    expect(math.minmax(a, 1, Math.min)).toEqual([0., 4., 3.])
  })

  test('should get max of the matrix along the axis', () => {
    const a = [[3., 0., 0.],
      [4., 5., 6.],
      [7., 3., 4.]]
    expect(math.minmax(a, 0, Math.max)).toEqual([7., 5., 6.])
  })


})