import {getGeometryFiles_testing, parseGeometryInFileFormat} from "../../common/util.js"
import Structure from "../../common/Structure.js"
import * as fs from 'fs'

const FILES_FOLDER = 'src/__tests__/geometryFileExport/'
const TEST_OUTPUT_FILE_ENDING = '.test_output.json'

const FILE_NAMES = [
  'geometry_periodic_cartesian.in', 
  'geometry_periodic_fractional.in', 
  'geometry_molecule_cartesian.in',
  'input_periodic_cartesian.xml', 
  'input_periodic_fractional.xml', 
  'input_molecule_cartesian.xml'
]

const periodicStructure = getStructureFromJson(JSON.parse(fs.readFileSync(FILES_FOLDER+'periodic.json', "utf8")))
const moleculeStructure = getStructureFromJson(JSON.parse(fs.readFileSync(FILES_FOLDER+'molecule.json', "utf8")))
//console.log('periodicStructure', periodicStructure)

const fileTestIOMap = new Map()

FILE_NAMES.forEach( fileName => {
  let content = fs.readFileSync(FILES_FOLDER+fileName, "utf8")
  fileTestIOMap.set(fileName, { content: content })
})

const periodicFilesFHIaims = getGeometryFiles_testing(periodicStructure)
fileTestIOMap.get('geometry_periodic_cartesian.in').testInput = periodicFilesFHIaims[0]
fileTestIOMap.get('geometry_periodic_fractional.in').testInput = periodicFilesFHIaims[1]
fileTestIOMap.get('geometry_molecule_cartesian.in').testInput = getGeometryFiles_testing(moleculeStructure)[0]

const periodicFilesExciting = getGeometryFiles_testing(periodicStructure, false)
fileTestIOMap.get('input_periodic_cartesian.xml').testInput = periodicFilesExciting[0]
fileTestIOMap.get('input_periodic_fractional.xml').testInput = periodicFilesExciting[1]
fileTestIOMap.get('input_molecule_cartesian.xml').testInput = getGeometryFiles_testing(moleculeStructure, false)[0]


fileTestIOMap.forEach( ( testIO, fileName) => {
  test('File '+fileName+' generated from Structure', () => {
    expect(testIO.testInput).toEqual(testIO.content)
  })
})


function getStructureFromJson(json){
  let structure = new Structure()
  structure.latVectors = json.latVectors
  structure.atoms = json.atoms
  return structure
}

/*
  let structure = parseGeometryInFileFormat(fs.readFileSync(FILES_FOLDER+'geometry_periodic_cartesian.in', "utf8"))
  console.log('structure', structure)
  fs.writeFile(FILES_FOLDER+'geometry_periodic_cartesian.json', JSON.stringify(structure))

  let structure = parseGeometryInFileFormat(fs.readFileSync(FILES_FOLDER+'geometry_molecule_cartesian.in', "utf8"))
  console.log('structure', structure)
  fs.writeFile(FILES_FOLDER+'molecule.json', JSON.stringify(structure))
*/



