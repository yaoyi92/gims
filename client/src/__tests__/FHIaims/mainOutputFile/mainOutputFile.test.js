import OutputAims from "../../../output-analyzer-mod/OutputAims.js"
import * as fs from 'fs'

const FILES_FOLDER = 'src/__tests__/FHIaims/mainOutputFile/'
const TEST_OUTPUT_FILE_ENDING = '.test_output.json'

//let fileName = 'file:///builds/gims-developers/gims/client/src/__tests__/aims.200118.out'

// const FILE_NAME = ['aims.out_normal_molec']

const FILE_NAMES = [
  'aims.200118.out',
  'aims.out_normal_molec',
  'aims.out_normal_period',
  'aims.out_normal_relax_molec',
  'aims.out_normal_relax_period',
  'aims.out_normal_relax_stress_period',
  'aims.out_normal_spin_molec',
  'aims.out_normal_spin_period',
  'aims.out_normal_spin_relax_molec',
  'aims.out_normal_spin_relax_period',
  'aims.out_normal_spin_relax_stress_period',
  'aims.out_MDlight_molec',
  'aims.out_MDlight_relax_molec',
  'aims.out_MDlight_relax_period',
  'aims.out_MDlight_relax_stress_period',
  'aims.out_MDlight_spin_molec',
  'aims.out_MDlight_spin_relax_molec'
]

 // FILE_NAMES.forEach( fileName => {
 //   let output = new OutputAims()
 //   output.parseFile(fileName, fs.readFileSync(FILES_FOLDER+fileName, "utf8"))
 //   console.log(FILES_FOLDER+fileName+TEST_OUTPUT_FILE_ENDING)
 //   fs.writeFile(FILES_FOLDER+fileName+TEST_OUTPUT_FILE_ENDING, JSON.stringify(output.scfLoops),()=>{})
 // })

const outputFileMap = new Map()

FILE_NAMES.forEach( fileName => {

  outputFileMap.set(fileName,
  	{
  	  content: fs.readFileSync(FILES_FOLDER+fileName, "utf8"),
  	  testOutput: JSON.parse(fs.readFileSync(FILES_FOLDER+fileName+TEST_OUTPUT_FILE_ENDING, "utf8"))
  	})
})


FILE_NAMES.forEach( fileName => {

  test('Output file: '+fileName+' - Object from parsing: scfLoops', () => {

    const fileContent = outputFileMap.get(fileName).content
    let outputInstance = new OutputAims()
    outputInstance.parseFile(fileName, fileContent)
    const scfLoopsTestInput = JSON.parse(JSON.stringify(outputInstance.scfLoops))
    const scfLoopsTestOutput = outputFileMap.get(fileName).testOutput

    for (let i = 0; i < scfLoopsTestInput.length; i++) {
   	  const loopData = scfLoopsTestInput[i]//outputInstance.scfLoops[i]
      for (const atom of loopData.structure.atoms) delete atom.radius
	  expect(loopData.finalScfEnergies).toEqual(scfLoopsTestOutput[i].finalScfEnergies)
	  expect(loopData.forces).toEqual(scfLoopsTestOutput[i].forces)
      if (loopData.maxForceComponent !== undefined)
        expect(loopData.maxForceComponent).toBeCloseTo(scfLoopsTestOutput[i].maxForceComponent, 5)
	  expect(loopData.structure).toEqual(scfLoopsTestOutput[i].structure)
	  expect(loopData.iterations).toEqual(scfLoopsTestOutput[i].iterations)
	  expect(loopData.isConverged).toEqual(scfLoopsTestOutput[i].isConverged)

	  /* This way Jest doesn't indicate the inner object failing
   	  for (let key in loopData) {
   	  	expect(loopData[key]).toEqual(scfLoopsTestOutput[i][key])
   	  } */
	}

  })

})


/*

export function getTotalEnergies(fileName){
	let fileText = outputFileMap.get(fileName)

	let output = new OutputAims()

	output.parseFile(fileName, fileText)
	let results_len = output.results.length - 1
	let result = output.results[results_len].scEnergy[0]

	let final_energies = []

	for (let i in output.results) {
	  final_energies.push(output.results[i].scEnergy[0])
	}
	return final_energies
}



test('Parsing total energies', () => {

  const TOTAL_ENERGIES = [
    -26538.0001145353, -26538.0765095849, -26538.0791245732, -26538.0797971265, -26538.0800593517,
    -26538.0802139604, -26538.0802834651, -26538.0803327913, -26538.0803516324, -26538.0803610969, -26538.0803665197
  ]
  expect(getTotalEnergies('aims.200118.out')).toEqual(TOTAL_ENERGIES)
})
*/
