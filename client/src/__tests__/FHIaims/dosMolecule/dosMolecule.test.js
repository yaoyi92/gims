import OutputAims from "../../../output-analyzer-mod/OutputAims.js"
import * as fs from 'fs'

const FILES_FOLDER = 'src/__tests__/FHIaims/dosMolecule/'
const TEST_OUTPUT_FILE_ENDING = '.test_output.json'

const FILE_NAMES = [
  'aims.out_dos_molec',
  'control.in',
  'geometry.in',
  'KS_DOS_total.dat',
]

const fileObjectArray = []  // Prepare the array of files for parseFiles()

FILE_NAMES.forEach( fileName => {
  let content = fs.readFileSync(FILES_FOLDER+fileName, "utf8")
  fileObjectArray.push( {name: fileName, content: content} )
})
const dosTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+'KS_DOS_total.dat'+TEST_OUTPUT_FILE_ENDING, "utf8"))
const scfLoopsTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+'aims.out_dos_molec'+TEST_OUTPUT_FILE_ENDING, "utf8"))

let outputInstance = new OutputAims()

test('Parse files', () => {
	outputInstance.parseFiles(fileObjectArray)
	expect(getParsedFiles(outputInstance.files)).toEqual(FILE_NAMES)
})

test('Parse output file', () => {
  outputInstance.parseOutputFile()
  // test output file generation -> fs.writeFile(FILES_FOLDER+'aims.out_dos_molec'+TEST_OUTPUT_FILE_ENDING, JSON.stringify(outputInstance.scfLoops),() => {})
  testScfLoopData(outputInstance, scfLoopsTestOutput)
})

test('DOS data', () => {
  let dosData = outputInstance.getFirstFileDosData()
  // test output file generation -> fs.writeFile(FILES_FOLDER+'DOS_data'+TEST_OUTPUT_FILE_ENDING, JSON.stringify(dosData),() => {})
  expect(dosData).toEqual(dosTestOutput)
})



function getParsedFiles(filesObject){

  const files = []
  filesObject.output.forEach( (c, fileName) => files.push(fileName) )
  filesObject.input.forEach( (c, fileName) => files.push(fileName) )
  filesObject.dos.forEach( (c, fileName) => files.push(fileName) )
  return files
}


function testScfLoopData(output, scfLoopsTestOutput){

  const scfLoopsTestInput = JSON.parse(JSON.stringify(output.scfLoops))

  for (var i = 0; i < scfLoopsTestInput.length; i++) {
    const loopData = scfLoopsTestInput[i]//outputInstance.scfLoops[i]
    for (const atom of loopData.structure.atoms) delete atom.radius
    expect(loopData.finalScfEnergies).toEqual(scfLoopsTestOutput[i].finalScfEnergies)
    expect(loopData.forces).toEqual(scfLoopsTestOutput[i].forces)
    expect(loopData.maxForceComponent).toEqual(scfLoopsTestOutput[i].maxForceComponent)
    expect(loopData.structure).toEqual(scfLoopsTestOutput[i].structure)
    expect(loopData.iterations).toEqual(scfLoopsTestOutput[i].iterations)
    expect(loopData.isConverged).toEqual(scfLoopsTestOutput[i].isConverged)
  }
}
