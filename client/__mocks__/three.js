
jest.mock('three', () => {
  const THREE = jest.requireActual('three');
  return {
    ...THREE,
    WebGLRenderer: jest.fn().mockReturnValue({
      // domElement: document.createElement('div'), // create a fake div
      setSize: jest.fn(),
      render: jest.fn(),
    }),
  };
});

jest.mock('three/examples/jsm/controls/TrackballControls', () => {})
jest.mock('three/examples/jsm/geometries/ConvexGeometry', () => {})

