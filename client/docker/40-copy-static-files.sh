#!/bin/sh

# Moves static files from /static to static volume at container startup
cp /static/* /usr/share/nginx/html/static/
